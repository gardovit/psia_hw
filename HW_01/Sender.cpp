#include "Sender.h"



Sender::Sender()
{
	printf("Input relative file path: ");
	std::cin >> filename;

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		printf("Error during WSAStarup\n");
		return;
	}
		
	// Configurate local port
	fromlen = sizeof(from);
	local.sin_family = AF_INET;
	local.sin_port = htons(local_port);
	local.sin_addr.s_addr = INADDR_ANY;

	win_socket = socket(AF_INET, SOCK_DGRAM, 0);

	//Timeout just a minimum amount	
	DWORD read_timeout = PACKAGE_DELAY;
	setsockopt(win_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&read_timeout, sizeof(read_timeout));

	if (bind(win_socket, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		return;
	}

	//Configurate target adress
	int ip_ret = mbstowcs(ip_addr_w, ip_addr, strlen(ip_addr));
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(target_port);
	InetPton(AF_INET, ip_addr_w, &addrDest.sin_addr.s_addr);

	sendData();
}

void Sender::shiftPackets() {
	Packet* tmp = packetList[0];
	for (int i = 0; i < MAX_ACTIVE_PACKATES - 1; i++) {
		packetList[i] = packetList[i + 1];
	}
	packetList[MAX_ACTIVE_PACKATES - 1] = tmp;
}

void Sender::sendData()
{
	//Prepare file and get its size
	FILE* file = fopen(filename.c_str(), "rb");
	if (!file) {
		printf("File couldn't be openned.\n");
		return;
	}
	
	fseek(file, 0, SEEK_END);
	int file_size = ftell(file);	
	rewind(file);

	//Prepare data for START packet
	*(unsigned int*)&transferBuffer[1] = file_size;
	transferBuffer[5] = UNIQUE_TRANSACTION_ID;
	*(unsigned short*)&transferBuffer[6] = (unsigned short)filename.length();
	for (unsigned int i = 0; i < filename.length(); i++) {
		transferBuffer[8 + i] = filename[i];
	}
	int size = 8 + filename.length();

	unsigned int crc = 0;
	CRC::crc32(transferBuffer, size, &crc);
	*(unsigned int*)&transferBuffer[size] = crc;
	size += 4;
	
	struct timeval read_timeout;
	read_timeout.tv_sec = 0;
	read_timeout.tv_usec = 10;

	//Send until "OK" is recieve
	while (true)
	{
		sendto(win_socket, transferBuffer, size, 0, (sockaddr*)&addrDest, sizeof(addrDest));
		memset((void*)recieveBuffer, 0, BUFFERS_LEN);
		int bytes = recvfrom(win_socket, recieveBuffer, sizeof(recieveBuffer), 0, (sockaddr*)&from, &fromlen);
		if (bytes > 0 && recieveBuffer[0] == 'O' && recieveBuffer[1] == 'K') {
			break;
		}
	}	
		
	//-----------------------------------------------------
	//How many confirmation packets should get recieved
	int packageCountDown = (int)ceil(file_size / 1000.0);
	//How many packets should be created yet
	int sendCountDown = packageCountDown;
	int index_counter = 0;
	unsigned long hash = 0;
	std::chrono::high_resolution_clock::time_point lastTimeSend = std::chrono::high_resolution_clock::now();
	
	for (int i = 0; i < MAX_ACTIVE_PACKATES; i++) {
		packetList[i] = new Packet;
	}

	FD_SET ReadSet;
	

	while (true) {

		//Should end?
		if (packageCountDown <= 0) {
			break;
		}

		//Delay for sending too fast - 1 ms
		std::chrono::high_resolution_clock::time_point actualTime = std::chrono::high_resolution_clock::now();	
		std::chrono::duration<double, std::milli> difference = actualTime - lastTimeSend;
		
		if (difference.count() > 1) {
			
			for (int i = 0; i < MAX_ACTIVE_PACKATES; i++) {

				Packet* packet = packetList[i];

				//Move send window if possible
				if (i == 0 && packet->recieved) {
					shiftPackets();
					Packet* last = packetList[MAX_ACTIVE_PACKATES - 1];

					last->index = index_counter++;
					last->send = false;
					last->recieved = false;

					i--;
					continue;
				}
				
				if (packet->send == false) {
					if (sendCountDown <= 0) {
						continue;
					}
					//
					//Create packet
					//

					packet->transferBuffer[0] = UNIQUE_TRANSACTION_ID;
					*(unsigned int*)&packet->transferBuffer[1] = packet->index;
					size = fread((void*)&packet->transferBuffer[5], sizeof(char), 1000, file);

					for (int i = 0; i < size; i++) {
						hash += *(unsigned char*)&packet->transferBuffer[5 + i];
					}
					size += 5;

					crc = 0;
					CRC::crc32(packet->transferBuffer, size, &crc);
					*(unsigned int*)&packet->transferBuffer[size] = crc;
					size += 4;

					packet->size = size;

					printf("Sending packet ID: %d\n", packet->index);

					//
					//SEND packet
					//
					sendto(win_socket, packet->transferBuffer, packet->size, 0, (sockaddr*)&addrDest, sizeof(addrDest));
					packet->send = true;

					//Save time
					packet->sendTime = std::chrono::high_resolution_clock::now();

					sendCountDown--;
					lastTimeSend = std::chrono::high_resolution_clock::now();
					break;
				}

				

				if (packet->send && packet->recieved == false) {
					//
					//Check whether it is time to resend
					//
					std::chrono::duration<double, std::milli> timeDiff = actualTime - packet->sendTime;
					if (timeDiff.count() > PACKET_TIMEOUT || packet->sendAgain) {
						printf("Re-sending: %d\n", packet->index);
						sendto(win_socket, packet->transferBuffer, packet->size, 0, (sockaddr*)&addrDest, sizeof(addrDest));
						packet->sendTime = std::chrono::high_resolution_clock::now();
						lastTimeSend = std::chrono::high_resolution_clock::now();
						packet->sendAgain = false;
						break;
					}
				}

			}
		}
				
		//	
		//Recieve confirmation packet
		//
		memset((void*)recieveBuffer, 0, BUFFERS_LEN);
		
		FD_ZERO(&ReadSet);
		FD_SET(win_socket, &ReadSet);
		
		int Total = 0;
		if ((Total = select(0, &ReadSet, NULL, NULL, &read_timeout)) == SOCKET_ERROR){
			printf("select() returned with error %d\n", WSAGetLastError());
		}


		if (FD_ISSET(win_socket, &ReadSet)) {
			int bytes = recvfrom(win_socket, recieveBuffer, sizeof(recieveBuffer), 0, (sockaddr*)&from, &fromlen);
			if (bytes >= 6 && recieveBuffer[0] == 'O' && recieveBuffer[1] == 'K') {

				int ID = *(int*)&recieveBuffer[2];
				printf("Recieving: %d\n", ID);
				for (int i = 0; i < MAX_ACTIVE_PACKATES; i++) {
					Packet* packet = packetList[i];
					if (packet->index == ID && packet->recieved == false) {
						packet->recieved = true;
						packageCountDown--;
						break;
					}
				}
			}
			else if (bytes >= 6 && recieveBuffer[0] == 'O' && recieveBuffer[1] == 'E') {
				int ID = *(int*)&recieveBuffer[2];
				printf("ERROR: %d\n", ID);
				for (int i = 0; i < MAX_ACTIVE_PACKATES; i++) {
					Packet* packet = packetList[i];
					if (packet->index == ID && packet->recieved == false) {
						packet->sendAgain = true;
						break;
					}
				}
			}
		}

		
		
	}
	fclose(file);
		
	for (int i = 0; i < MAX_ACTIVE_PACKATES; i++) {
		delete packetList[i];
	}

	//-----------------------------------------------------
	
	//Prepare ENDING packet
	transferBuffer[0] = UNIQUE_TRANSACTION_ID;
	*(unsigned int*)&transferBuffer[1] = INT_MAX;
	*(unsigned long*)&transferBuffer[5] = hash;
	size = 13;

	crc = 0;
	CRC::crc32(transferBuffer, size, &crc);
	*(unsigned int*)&transferBuffer[size] = crc;
	size += 4;

	//Send END packet until "OK" (file is fine) or "FILE ERROR" (hash doesnt't match) is recieved
	while (true){
		sendto(win_socket, transferBuffer, size, 0, (sockaddr*)&addrDest, sizeof(addrDest));
		memset((void*)recieveBuffer, 0, BUFFERS_LEN);
		int bytes = recvfrom(win_socket, recieveBuffer, sizeof(recieveBuffer), 0, (sockaddr*)&from, &fromlen);
		if (bytes > 0 && ((recieveBuffer[0] == 'O' && recieveBuffer[1] == 'K') || strcmp(recieveBuffer, "FILE ERROR") == 0)){
			break;
		}
	}

	closesocket(win_socket);
}