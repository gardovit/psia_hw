#pragma once

#include "dependencies.h"
#include "Packet.h"


class Sender {
public:
	Sender();
	
	void sendData();


private:

	void shiftPackets();

	wchar_t ip_addr_w[16] = { 0 };

	const char* ip_addr = SENDER_DEST_IP;
	int target_port = SENDER_DEST_PORT;
	int local_port = SENDER_LOCAL_PORT;

	Packet* packetList[MAX_ACTIVE_PACKATES];

	char transferBuffer[BUFFERS_LEN] = { 0 };
	char recieveBuffer[BUFFERS_LEN] = { 0 };
	
	std::string filename;
	
	//winsock varaibles for recieving
	struct sockaddr_in local;
	struct sockaddr_in from;
	int fromlen = 0;
	SOCKET win_socket;

	//winsock varaibles for sending
	sockaddr_in addrDest;

};

