#include "Reciever.h"

Reciever::Reciever()
{
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		printf("Error during WSAStarup\n");
		return;
	}

	// SEND PORT
	fromlen = sizeof(from);
	local.sin_family = AF_INET;
	local.sin_port = htons(local_port);
	local.sin_addr.s_addr = INADDR_ANY;

	win_socket = socket(AF_INET, SOCK_DGRAM, 0);
	//Delay
	DWORD read_timeout = PACKAGE_DELAY;
	setsockopt(win_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&read_timeout, sizeof read_timeout);

	if (bind(win_socket, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		return;
	}

	//Init target adress
	int ip_ret = mbstowcs(ip_addr_w, ip_addr, strlen(ip_addr));
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(target_port);
	InetPton(AF_INET, ip_addr_w, &addrDest.sin_addr.s_addr);
	
	startReceiving();
}

void Reciever::send(const char* message) {
	memset((void*)sendingBuffer, 0, BUFFERS_LEN);
	strcpy(sendingBuffer, message);
	sendto(win_socket, sendingBuffer, strlen(sendingBuffer), 0, (sockaddr*)&addrDest, sizeof(addrDest));
}

void Reciever::send(const char* message, unsigned int index) {
	memset((void*)sendingBuffer, 0, BUFFERS_LEN);
	strcpy(sendingBuffer, message);
	int size = strlen(sendingBuffer) + 4;
	*(unsigned int*)&sendingBuffer[strlen(sendingBuffer)] = index;
	sendto(win_socket, sendingBuffer, size, 0, (sockaddr*)&addrDest, sizeof(addrDest));
}

int Reciever::waitForStartsequence() {

	//Wait until START packet is recieved
	while (true) {

		memset((void*)transferBuffer, 0, BUFFERS_LEN);
		int bytes = recvfrom(win_socket, transferBuffer, sizeof(transferBuffer), 0, (sockaddr*)&from, &fromlen);

		if (bytes > 7) {
			if (transferBuffer[0] != 0) { continue; }

			fileSize = *(unsigned int*)&transferBuffer[1];
			uniqueID = *(unsigned char*)&transferBuffer[5];
			filename_lenght = *(unsigned short*)&transferBuffer[6];

			if (filename_lenght == 0) { continue; }

			filename = new char[filename_lenght + 1];
			for (unsigned int i = 0; i < filename_lenght; i++) {
				filename[i] = transferBuffer[8 + i];
			}
			filename[filename_lenght] = '\0';

			//We can start recieving
			unsigned int crc = 0;
			CRC::crc32(transferBuffer, bytes - 4, &crc);

			if (crc == *((unsigned int*)(&transferBuffer[bytes - 1 - 3]))) {
				//Notify about succesful transfer
				send("OK");
				break;
			}
			else {
				send("ERROR");
			}
		}
	}
	
	return 0;
}

void Reciever::startReceiving() {

	waitForStartsequence();

	transferedData = new unsigned char[fileSize];
	memset((void*)transferedData, 0, fileSize);

	printf("Recieving file...\n");

	int transferSuccess = 0;
	while (true) {

		//Read package data
		memset((void*)transferBuffer, 0, BUFFERS_LEN);
		int bytes = recvfrom(win_socket, transferBuffer, sizeof(transferBuffer), 0, (sockaddr*)&from, &fromlen);

		if (bytes >= 7 && transferBuffer[0] == 0) {
			send("OK");
		}

		if (bytes >= 5 && transferBuffer[0] == uniqueID) {

			int index = *(int*)&transferBuffer[1];

			//Check for END packet
			if (index == INT_MAX) {

				unsigned int crc = 0;
				CRC::crc32(transferBuffer, bytes - 4, &crc);

				//Check CRC
				if (crc == *((unsigned int*)(&transferBuffer[bytes - 1 - 3]))) {

					// Hash check
					unsigned long hash = 0;
					for (int i = 0; i < fileSize; i++) {
						hash += transferedData[i];
					}

					if (hash == *((unsigned long*)&transferBuffer[5])) {
						send("OK");
						transferSuccess = 1;
					}
					else {
						send("FILE ERROR"); // hash nesouhlas�, p�enos se p�esto ukon��
						transferSuccess = 0;
					}
					break;
				}
				else {
					send("ERROR");
				}
			}

			int data_size = bytes - 5;
			if (data_size <= 0) {
				//no useful data in packet
				printf("No data in packet\n");
				continue;
			}
			
			//CRC check
			unsigned int crc = 0;
			CRC::crc32(transferBuffer, bytes - 4, &crc);

			if (crc == *((unsigned int*)(&transferBuffer[bytes - 1 - 3]))) {
				//save to tranferedData
				char* data = (char*)&transferBuffer[5];
				memcpy((void*)&transferedData[1000 * index], (void*)data, data_size -4);
				send("OK", index); //Notify Sender about succesful transfer

				printf("Recieved OK: %d\n", index);

			}
			else {
				//We cannot be sure if index is not corrupted so we cannot send
				send("OE", index);

				printf("Recieved ERROR: %d\n", index);
			}
		}
	}

	//What until we know that our last OK was recived by Sender. 
	//Continue when we timeout
	int waitingCount = 20;
	while (true) {
		memset((void*)transferBuffer, 0, BUFFERS_LEN);
		int bytes = recvfrom(win_socket, transferBuffer, sizeof(transferBuffer), 0, (sockaddr*)&from, &fromlen);
		if (bytes > 0) {
			send("OK");
		}
		else {
			waitingCount--;
			if (waitingCount == 0) {
				break;
			}
		}
	}

	closesocket(win_socket);

	//save to file
	if (transferSuccess) {
		FILE* file = fopen(filename, "wb");
		fwrite(transferedData, 1, fileSize, file);
		fclose(file);
		printf("File \"%s\" recieved.\n", filename);
	}
	
	//Free memory
	delete [] filename;
	delete [] transferedData;
}

