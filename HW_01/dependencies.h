#pragma once
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "ws2_32.lib")


#include "crc.h"
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"

#include <stdio.h>
#include <malloc.h>
#include <bitset>

#include <cstdlib>
#include <iostream>
#include <string>
#include <chrono>

#include <Windows.h>

#define BUFFERS_LEN 1024
#define PACKAGE_DELAY 100
#define UNIQUE_TRANSACTION_ID 70

#define SENDER_LOCAL_PORT 15001
#define SENDER_DEST_PORT 14000
#define SENDER_DEST_IP "127.0.0.1"

#define RECIEVER_LOCAL_PORT 15000
#define RECIEVER_DEST_PORT 14001
#define RECIEVER_DEST_IP "192.168.30.14"

#define MAX_ACTIVE_PACKATES 5
#define PACKET_TIMEOUT 100

