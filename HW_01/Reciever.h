#pragma once
#include "dependencies.h"

class Reciever {
public:
	Reciever();
	
	int waitForStartsequence();
	void startReceiving();

	//Send message to target
	void send(const char* message);
	void send(const char* message, unsigned int index);

private:
	unsigned char* transferedData = nullptr;
	char transferBuffer[BUFFERS_LEN] = { 0 };
	char sendingBuffer[BUFFERS_LEN] = { 0 };
	
	const char* ip_addr = RECIEVER_DEST_IP;
	wchar_t ip_addr_w[16] = { 0 };

	int target_port = RECIEVER_DEST_PORT;
	int local_port = RECIEVER_LOCAL_PORT;

	unsigned int filename_lenght = 0;
	char* filename = nullptr;
	unsigned int fileSize = 0;
	unsigned char uniqueID = 0;
	
	//winsock varaibles for recieving
	struct sockaddr_in local;
	struct sockaddr_in from;
	int fromlen = 0;
	SOCKET win_socket;

	//winsock varaibles for sending
	sockaddr_in addrDest;

};
